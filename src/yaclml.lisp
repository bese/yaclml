;;;; -*- lisp -*-
;;;; $Id: yaclml.lisp,v 1.3 2003/08/17 16:11:16 mbaringer Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.yaclml)

(defvar *yaclml-stream* t)

(defmacro with-yaclml-stream ((stream &key &allow-other-keys) &body body)
  `(let ((*yaclml-stream* ,stream))
     (declare (special *yaclml-stream*))
     ,@body))

(defvar *yaclml-tags* (make-hash-table :test 'eql))

(defun yaclml-princ (item)
  "Request that the result of the form ITEM be printed at run item."
  (declare (special tag-code %indent%))
  (if (or (stringp item)
          (characterp item)
          (numberp item))
      (push (princ-to-string item) tag-code)
      (push `(princ ,item *yaclml-stream*) tag-code)))

(defun yaclml-collect (form)
  "Request that CODE be part of the tag's code."
  (declare (special tag-code))
  (push form tag-code))

(defun yaclml-princ-attributes (attributes)
  (let ((constants '())
        (runtime '()))
    ;; we move the attributes around in order to get the longest constant sequence we can
    (dolist (attr attributes)
      (when (cdr attr) ;; this implies that passed attributes with a nil value are not printed
        (if (constantp (cdr attr))
            (push attr constants)
            (push attr runtime))))
    (dolist (attr (append constants runtime))
      (yaclml-princ " ")
      (yaclml-princ (car attr))
      (unless (eql t (cdr attr))
        (yaclml-princ "=\"")
        (yaclml-princ (cdr attr))
        (yaclml-princ "\"")))))

(defun open-tag (name indentp &rest attributes)
  "Used within a DEFTAG makes the proper calls to YACLML-PRINC and
  YACLML-COLLECT such that an open tag will be printed when the code
  is finally run."
  (declare (special %indent%))
  (when indentp
    (yaclml-princ #\Newline)
    (yaclml-princ (make-string %indent% :initial-element #\Space))
    (incf %indent%))
  (yaclml-princ "<")
  (yaclml-princ name)
  (yaclml-princ-attributes attributes)
  (yaclml-princ ">"))

(defun close-tag (name indentp)
  (declare (special %indent%))
  (when indentp
    (decf %indent%)
    (yaclml-princ #\Newline)
    (yaclml-princ (make-string %indent% :initial-element #\Space)))
  (yaclml-princ "</")
  (yaclml-princ name)
  (yaclml-princ ">"))

(defun empty-tag (name indentp &rest attributes)
  (declare (special %indent%))
  (when indentp
    (yaclml-princ (make-string %indent% :initial-element #\Space)))
  (yaclml-princ "<")
  (yaclml-princ name)
  (yaclml-princ-attributes attributes)
  (yaclml-princ "/>"))

(defun yaclml-do-body (body)
  (dolist (form body)
    (cond
      ((and (consp form)
            (gethash (car form) *yaclml-tags*))
       (funcall (gethash (car form) *yaclml-tags*) (cdr form)))
      ((and (consp form)
            (eql 'yaclml-quote (car form)))
       (dolist (b (cdr form))
         (yaclml-do-body (list b))))
      ((or (stringp form)
           (characterp form))
       (yaclml-princ (escape-as-html (string form))))
      (t
       (yaclml-collect form)))))

(defmacro deftag (name attributes &body body)
  (let ((contents (gensym)))
    `(progn
       (setf (gethash ',name *yaclml-tags*)
             (lambda (,contents)
               (attribute-bind ,attributes ,contents
                 ,@body)))
       (defmacro ,name (&rest contents)
         (let ((tag-code nil))
           (declare (special tag-code %indent%))
            ;; build tag's body
           (if (boundp '%indent%)
               (funcall (gethash ',name *yaclml-tags*) contents)
               (let ((%indent% 0))
                 (declare (special %indent%))
                 (funcall (gethash ',name *yaclml-tags*) contents)))
           (setf tag-code (nreverse tag-code))
           `(progn ,@(mapcar (lambda (form)
                               (if (stringp form)
                                   `(princ ,form *yaclml-stream*)
                                   form))
                             (fold-strings tag-code))))))))

(defmacro within-tag ((name-string &rest attributes) &rest body)
  (if body
      `(progn
         (open-tag ,name-string nil ,@(loop while attributes
                                               collect `(cons ,(pop attributes) ,(pop attributes))))
         ,@body
         (close-tag ,name-string nil))
      `(empty-tag ,name-string nil ,@(loop while attributes
                                               collect `(cons ,(pop attributes) ,(pop attributes))))))

(defmacro defsimple-xtag (name indentp &rest attributes)
  (let ((name-string (string-downcase (string name))))
    `(deftag ,name (&attribute ,@attributes &body body)
       (if body
           (progn
             (open-tag ,name-string ,indentp
                       ,@(mapcar (lambda (attr-symbol)
                                   `(cons ,(string-downcase (string attr-symbol))
                                          ,attr-symbol))
                                 attributes))
             (yaclml-do-body body)
             (close-tag ,name-string ,indentp))
           (empty-tag ,name-string ,indentp ,@(mapcar (lambda (attr-symbol)
                                                        `(cons ,(string-downcase (string attr-symbol))
                                                               ,attr-symbol))
                                                      attributes))))))

(defun parse-attribute-spec (attribute-spec)
  (let* ((required '())
         (attrs '())
         (flags '())
         (body-var (gensym))
         (put (lambda (item)
                (push item required))))
    (dolist (attr attribute-spec)
      (case attr
        (&attribute (setf put (lambda (item)
                                (if (listp item)
                                    (case (length item)
                                      (1 (push (list (car item) nil) attrs))
                                      (2 (push item attrs))
                                      (t
                                       (error "Bad &attribute spec: ~S" item)))
                                    (push (list item nil) attrs)))))
        (&flag (setf put (lambda (item)
                           (push item flags))))
        (&body (setf put (lambda (item)
                           (setf body-var item))))
        (t (funcall put attr))))
    (list (nreverse required) (nreverse attrs) (nreverse flags) body-var)))

(defmacro attribute-bind (attribute-spec list &body body)
  "Attributes can contain &attribute, &flag and &body."
  (destructuring-bind (locals attrs flags body-var)
      (parse-attribute-spec attribute-spec)
    (let ((list-sym (gensym))
          (element-sym (gensym)))
      `(let ,(append locals attrs flags (list body-var) (list (list list-sym list)))
         (declare (ignorable ,body-var))
         ,@(loop
              for local in locals
              collect `(setf ,local (pop ,list-sym)))
         (while (and (consp ,list-sym)
                     (keywordp (car ,list-sym)))
           (case-bind ,element-sym (pop ,list-sym)
             ,@(loop
                  for attr in attrs
                  collect `(,(intern (string (car attr)) :keyword) (setf ,(car attr) (pop ,list-sym))))
             ,@(loop
                  for flag in flags
                  collect `(,(intern (string flag) :keyword) (setf ,flag t)))
             (t
              (error "Unrecognized attribute ~S in ~S" ,element-sym ,list-sym))))
         (setf ,body-var ,list-sym)
         ,@(if (and (consp body)
                   (consp (car body))
                   (eql 'declare (car (car body))))
               `((locally ,@body))
               body)))))

(defun |[ reader| (stream char)
  (declare (ignore char))
  (with-collector (forms)
    (loop
       for char = (read-char stream t nil t)
       do (case char
            (#\\ (forms `(yaclml-print ,(read-char stream t nil t))))
            (#\] (return-from |[ reader| `(yaclml-quote ,@(forms))))
            (#\, (forms (read stream t nil t)))
            (t (forms char))))))

(defvar *readers* '())

(defun enable-yaclml-syntax ()
  (push (get-macro-character #\[) *readers*)
  (set-macro-character #\[ #'|[ reader|))

(defun disable-yaclml-syntax ()
  (unless *readers*
    (error "Can't disable yaclml syntax if it hasn't been enabled."))
  (set-macro-character #\[ (pop *readers*))))
