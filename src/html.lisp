;;;; -*- lisp -*-
;;;_* File Info
;;;; $Id: html.lisp,v 1.1 2003/08/17 14:15:04 mbaringer Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.yaclml)

;;;_* Various Variables for generating HTML

(defvar *html-prologue* "<!DOCTYPE html
PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"
\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">")

;;;_* Helper code for generating HTML tags

(defmacro defhtml-tag (name-sym inline &rest attributes)
  (let (attrs)
    (loop for attr in attributes
         do (case attr
              (:coreattrs
               (push* attrs 'id 'class 'style 'title))
              (:events
               (push* attrs 'onclick 'ondblclick 'onmousedown 'onmouseup
                      'onmouseover 'onmousemove 'onmouseout 'onkeypress
                      'onkeydown 'onkeyup))
              (:i18n
               (push* attrs 'dir 'lang))
              (t
               (push* attrs attr))))
    `(defsimple-xtag ,name-sym ,(eql inline :block) ,@attrs)))

;;;_* HTML tags

(defhtml-tag <:A :inline :coreattrs :events href name rev ref target)

(defhtml-tag <:ABBREV :inline :coreattrs)

(defhtml-tag <:ACRONYM :inline :coreattrs)

(defhtml-tag <:ADDRESS :inline :coreattrs)

(defhtml-tag <:APPLET :block :coreattrs
  codebase code name align alt
  height width hspace vspace download)

(defhtml-tag <:AREA :block :coreattrs shape alt co-ords href target)

(defhtml-tag <:B :inline :coreattrs) 

(defhtml-tag <:BASE :block :coreattrs href target)

(defhtml-tag <:BASEFONT :block :coreattrs size face color)

(defhtml-tag <:BGSOUND :block :coreattrs src loop volume balance)

(defhtml-tag <:BIG :inline :coreattrs)

(defhtml-tag <:BLINK :inline :coreattrs) 

(defhtml-tag <:BLOCKQUOTE :block :coreattrs cite)

(defhtml-tag <:BODY :block :coreattrs bgcolor link vlink alink text
             background bgproperties topmargin leftmargin
             rightmargin bottommargin marginwidth marginheight)
                
(defhtml-tag <:BR :inline :coreattrs clear)

(defhtml-tag <:CAPTION :inline :coreattrs align valign)

(defhtml-tag <:CENTER :inline :coreattrs)

(defhtml-tag <:CITE :inline :coreattrs)

(defhtml-tag <:CODE :block :coreattrs) 

(defhtml-tag <:COL :block :coreattrs span width align valign bgcolor)

(defhtml-tag <:COLGROUP :block :coreattrs span width align valign bgcolor)

(defhtml-tag <:DEL :inline :coreattrs datetime cite)

(defhtml-tag <:DFN :inline :coreattrs)

(defhtml-tag <:DIR :inline :coreattrs)

(defhtml-tag <:DIV :block :coreattrs align)

(defhtml-tag <:DL :block :coreattrs)

(defhtml-tag <:DT :inline :coreattrs)

(defhtml-tag <:DD :inline :coreattrs)

(defhtml-tag <:EM :inline :coreattrs)

(defhtml-tag <:EMBED :block :coreattrs src pluginspace width height align
                type)

(defhtml-tag <:FONT :inline :coreattrs size face color point-size weight)

(defhtml-tag <:FORM :block :coreattrs action target method enctype name
                accept accept-charset)

(defhtml-tag <:FRAME :block :coreattrs src name scrolling norize width
                height marginwidth marginheight frameborder
                bordercolor)

(defhtml-tag <:FRAMESET :block :coreattrs cols rows border framespacing
                frameborder bordercolor)

(defhtml-tag <:H1 :inline :coreattrs align)

(defhtml-tag <:H2 :inline :coreattrs align)

(defhtml-tag <:H3 :inline :coreattrs align)

(defhtml-tag <:H4 :inline :coreattrs align)

(defhtml-tag <:H5 :inline :coreattrs align)

(defhtml-tag <:H6 :inline :coreattrs align)

(defhtml-tag <:HEAD :block :coreattrs profile)

(defhtml-tag <:HR :block :coreattrs align size width color)

(defhtml-tag <:HTML :block :coreattrs lang)

(defhtml-tag <:I :inline :coreattrs)

(defhtml-tag <:IFRAME :black :coreattrs src width height hspace vspace
                align name scrolling warginwidth warginheight
                frameborder bgcolor)

(defhtml-tag <:IMG :inline :coreattrs src width height hspace vspace
                border alt align usemap ismap lowsrc dynsrc loop
                longdesc)

(defhtml-tag <:INPUT :inline :coreattrs name value type size maxlength
                checked accept src align width height border alt
                usemap ismap)

(defhtml-tag <:INS :inline :coreattrs datetime cite)

(defhtml-tag <:ISINDEX :inline :coreattrs)

(defhtml-tag <:KBD :inline :coreattrs)

(defhtml-tag <:LANG :inline :coreattrs) 

(defhtml-tag <:LH :inline :coreattrs)

(defhtml-tag <:LI :block :coreattrs type value)

(defhtml-tag <:LINK :block :coreattrs rel type href rev target charset media)

(defhtml-tag <:LISTING :inline :coreattrs)

(defhtml-tag <:MAP :block :coreattrs name)

(defhtml-tag <:MARQUEE :block :coreattrs width height hspace vspace
             scrollAmount scrollDelay truespeed loop behavior
             direction bgcolor)

(defhtml-tag <:MATH :inline :coreattrs)

(defhtml-tag <:MENU :inline :coreattrs)

(defhtml-tag <:META :block :coreattrs name content http-equiv scheme)

(defhtml-tag <:MULTICOL :block :coreattrs cols gutter width)

(defhtml-tag <:NOBR :inline :coreattrs)

(defhtml-tag <:NOEMBED :block :coreattrs)

(defhtml-tag <:NOFRAMES :block :coreattrs)

(defhtml-tag <:NOLAYER :block :coreattrs)

(defhtml-tag <:NOSCRIPT :block :coreattrs)

(defhtml-tag <:NOTE :block :coreattrs)

(defhtml-tag <:OL :block :coreattrs type start)

(defhtml-tag <:OBJECT :block :coreattrs classid codebase code type data
                width height hspace vspace align)

(defhtml-tag <:OVERLAY :block :coreattrs)

(defhtml-tag <:OPTION :inline :coreattrs value selected)

(defhtml-tag <:P :block :coreattrs align)

(defhtml-tag <:PARAM :block :coreattrs valuetype name value)

(defhtml-tag <:PLAINTEXT :block :coreattrs)

(defhtml-tag <:PRE :inline :coreattrs)

(defhtml-tag <:Q :inline :coreattrs)

(defhtml-tag <:RANGE :inline :coreattrs)

(defhtml-tag <:S :inline :coreattrs)

(defhtml-tag <:SAMP :inline :coreattrs)

(defhtml-tag <:SCRIPT :block :coreattrs language type src for event defer
             charset)

(defhtml-tag <:SELECT :inline :coreattrs name size multiple)

(defhtml-tag <:SMALL :inline :coreattrs)

(defhtml-tag <:SPACER :inline :coreattrs type size width height)

(defhtml-tag <:SPAN :inline :coreattrs)

(defhtml-tag <:STRIKE :inline :coreattrs)

(defhtml-tag <:STRONG :inline :coreattrs)

(defhtml-tag <:STYLE :block :coreattrs media type)

(defhtml-tag <:SUB :inline :coreattrs)

(defhtml-tag <:SUP :inline :coreattrs)

(defhtml-tag <:TAB :inline :coreattrs)

(defhtml-tag <:TABLE :block :coreattrs border cellspacing cellpadding
             bgcolor background bordercolor bordercolorlight
             bordercolordark width height hspace vspace align cols
             frame rules summary)

(defhtml-tag <:TBODY :block :coreattrs align valign bgcolor)

(defhtml-tag <:TD :inline :coreattrs align valign bgcolor background
             bordercolor colspan rowspan width height headers scope
             abbr axis)

(defhtml-tag <:TEXTAREA :inline :coreattrs cols rows readonly wrap name)

(defhtml-tag <:TEXTFLOW :inline :coreattrs)

(defhtml-tag <:TFOOT :block :coreattrs) 

(defhtml-tag <:TH :inline :coreattrs) 

(defhtml-tag <:THEAD :inline :coreattrs) 

(defhtml-tag <:TITLE :inline :coreattrs) 

(defhtml-tag <:TR :block :coreattrs align valign width height bgcolor
             bordercolor)

(defhtml-tag <:TT :inline :coreattrs)

(defhtml-tag <:U :inline :coreattrs)

(defhtml-tag <:UL :inline :coreattrs type)
