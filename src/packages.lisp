;;;; -*- lisp -*-
;;;; $Id: packages.lisp,v 1.3 2003/08/17 16:11:16 mbaringer Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :common-lisp-user)

(defpackage :it.bese.yaclml
  (:use :common-lisp :it.bese.arnesi)
  (:export #:deftag
           #:within-tag
           #:defsimple-xtag
           #:process-yaclml-file
           #:*yaclml-stream*
           #:with-yaclml-stream
           #:yaclml-to-file
           #:enable-yaclml-syntax
           #:disable-yaclml-syntax))

(defpackage :it.bese.yaclml.tags
  (:nicknames :<)
  (:use)
  (:export #:a 
           #:abbrev 
           #:acronym 
           #:address 
           #:applet 
           #:area 
           #:author 
           #:b 
           #:banner 
           #:base 
           #:basefont 
           #:bgsound 
           #:big 
           #:blink 
           #:blockquote 
           #:bq 
           #:body 
           #:br 
           #:caption 
           #:center 
           #:cite 
           #:code 
           #:col 
           #:colgroup 
           #:credit 
           #:del 
           #:dfn 
           #:dir 
           #:div 
           #:dl 
           #:dt 
           #:dd 
           #:em 
           #:embed 
           #:fig 
           #:fn 
           #:font 
           #:form 
           #:frame 
           #:frameset 
           #:h1 
           #:h2 
           #:h3 
           #:h4 
           #:h5 
           #:h6 
           #:head 
           #:hr 
           #:html 
           #:i 
           #:iframe 
           #:img 
           #:input 
           #:ins 
           #:isindex 
           #:kbd 
           #:lang 
           #:lh 
           #:li 
           #:link 
           #:listing 
           #:map 
           #:marquee 
           #:math 
           #:menu 
           #:meta 
           #:multicol 
           #:nobr
           #:noembed
           #:noframes
           #:nolayer
           #:noscript
           #:note
           #:object
           #:ol
           #:option
           #:overlay 
           #:p 
           #:param 
           #:person 
           #:plaintext 
           #:pre 
           #:q 
           #:range
           #:s
           #:samp 
           #:script 
           #:select 
           #:small 
           #:spacer
           #:span
           #:spot 
           #:strike 
           #:strong
           #:style
           #:sub 
           #:sup 
           #:tab 
           #:table 
           #:tbody 
           #:td 
           #:textarea 
           #:textflow 
           #:tfoot 
           #:th 
           #:thead 
           #:title 
           #:tr 
           #:tt 
           #:u 
           #:ul 

           #:text
           #:submit
           #:image
           #:checkbox

           #:href
           #:stylesheet
           #:prin
           #:prin-raw
           #:page
           #:section
           #:itemize
           
           ;; usefull stuff for writing lisp code (requires a good style sheet)
           #:syntax
           #:description
           #:examples
           #:see-also
           #:arguments-and-values
           #:notes
           
           #:defun
           #:defmacro
           #:defclass
           #:defstruct
           #:define-condition
           #:defmethod
           #:defgeneric))

(defpackage :it.bese.yaclml.doc
  (:use :common-lisp :it.bese.yaclml)
  (:import-from :it.bese.yaclml.tags
                #:p #:page #:i #:b #:href))
  