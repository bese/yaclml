;;;; -*- lisp -*-
;;;; $Id: tags.lisp,v 1.1 2003/08/17 15:52:00 mbaringer Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.yaclml)

(deftag <:prin (thing)
  (yaclml-collect `(princ (escape-as-html (princ-to-string ,data)) *yaclml-stream*)))

(deftag <:prin-raw (thing)
  (yaclml-collect `(princ ,thing *yaclml-stream*)))

;;;_* HTML utility tags

;;;_ : General Markup stuff

(deftag <:href (url &body body)
  (within-tag ("a" "href" url)
    (if body
        (yaclml-do-body body)
        (yaclml-do-body (list url)))))

(deftag <:stylesheet (&attribute (sheet "style.css"))
  (within-tag ("link" "href" sheet "rel" "stylesheet" "type" "text/css" "title" "default")))

;;;_ : Forms

(deftag <:text (&attribute name value class &body body)
  (within-tag ("input" "type" "text" "name" name "value" value "class" class)
    (yaclml-do-body body)))

(deftag <:submit (&attribute onclick name value)
  (within-tag ("input" "type" "submit" "onClick" onClick "name" name "value" value)))

(deftag <:image (&attribute src name value height width id class)
  (within-tag ("input" "type" "image" "src" src "name" name "value"
               value "height" height "width" width "id" id "class" class)))

(deftag <:checkbox (&attribute name value &body body)
  (within-tag ("input" "type" "checkbox" "name" name "value" value)
    (yaclml-do-body body)))
